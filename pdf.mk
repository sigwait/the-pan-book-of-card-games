book := hubert,phillips__the_pan_book_of_card_games

.DELETE_ON_ERROR:
out := _out/pdf
mkdir = @mkdir -p $(dir $@)
points = $(shell echo '72 * $1' | bc)

img.color.src := $(addprefix img/, $(file < color.txt))
pdf.color.dest := $(patsubst img/%.jpg, $(out)/%.pdf, $(img.color.src))
img.bw.src := $(filter-out $(img.color.src), $(wildcard img/*.jpg))
pdf.bw.dest := $(patsubst img/%.jpg, $(out)/%.pdf, $(img.bw.src))

# inches
w := 4*2
h := 7.01*2

$(out)/$(book).pdf: $(pdf.color.dest) $(pdf.bw.dest) $(out)/pdfmarks
	gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite \
	 -dDEVICEWIDTHPOINTS=$(call points,$(w)) \
	 -dDEVICEHEIGHTPOINTS=$(call points,$(h)) \
	 -dPDFFitPage -dPDFSETTINGS=/prepress -sOutputFile=$@ $(sort $^)

$(out)/pdfmarks: meta.txt outline.lisp
	djvused2pdfmark meta < meta.txt > $@
	djvused2pdfmark < outline.lisp >> $@

$(pdf.color.dest): $(out)/%.pdf: img/%.jpg
	$(mkdir)
	img2pdf $< -o $@

$(out)/%.tiff: img/%.jpg
	$(mkdir)
	convert -scale 73% -brightness-contrast 25x55 -compress lzw -type Grayscale $< $@

$(pdf.bw.dest): %.pdf: %.tiff
	tesseract $< $* pdf
