# reqs

    dnf install djvulibre djvulibre-devel netpbm-progs tesseract

& install http://jwilk.net/software/ocrodjvu manually:

~~~
$ cd ~/opt/s
$ git clone https://github.com/jwilk-archive/ocrodjvu
~~~

It requires (Jan 2, 2022) python2.7. On f37 you need to download
`virtualenv.pyz` for python2.7, then create a virtualenv with it, pip
install all the deps which ocrodjvu will complain about

~~~
$ cd ~/opt/s
$ wget https://bootstrap.pypa.io/virtualenv/2.7/virtualenv.pyz -O virtualenv.2.7pyz
$ python2 virtualenv.2.7.pyz ocrodjvu-virtualenv
$ pip install lxml python-djvulibre subprocess32
~~~

then use

~~~
$ cat ~/bin/ocrodjvu
#!/bin/sh

source ~/opt/s/ocrodjvu-virtualenv/bin/activate
~/opt/s/ocrodjvu/ocrodjvu "$@"
~~~
