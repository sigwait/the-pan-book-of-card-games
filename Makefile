book := hubert,phillips__the_pan_book_of_card_games

.DELETE_ON_ERROR:
out := _out/djvu
mkdir = @mkdir -p $(dir $@)

img.color.src := $(addprefix img/, $(file < color.txt))
djvu.color.dest := $(patsubst img/%.jpg, $(out)/%.djvu, $(img.color.src))
img.bw.src := $(filter-out $(img.color.src), $(wildcard img/*.jpg))
djvu.bw.dest := $(patsubst img/%.jpg, $(out)/%.djvu, $(img.bw.src))
meta := outline.lisp meta.txt

$(out)/$(book).djvu: $(djvu.color.dest) $(djvu.bw.dest) $(meta)
	djvm -c $@ $(sort $(filter %.djvu,$^))
	djvused -e 'set-outline outline.lisp' $@ -s
	djvused -e 'set-meta meta.txt' $@ -s

%.djvu: %.tiff
	cjb2 -lossy $< $@
	ocrodjvu --in-place $@

$(djvu.color.dest): %.djvu: %.ppm
	cpaldjvu -dpi 300 -colors 16 $< $@

# color
$(out)/%.ppm: img/%.jpg
	$(mkdir)
	jpegtopnm $< 2>/dev/null > $@

# bitonal
threshold.def := 0.5
threshold = $(or \
	$(word 2,$(shell grep -v '\s*\#' tiff.conf | grep $(notdir $<))), \
	$(threshold.def))
$(out)/%.tiff: img/%.jpg
	$(mkdir)
	jpegtopnm $< 2>/dev/null | pamthreshold -simple -threshold=$(threshold) | pamtotiff > $@
