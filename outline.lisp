(bookmarks
 ("Foreword" "#4")
 ("Introduction" "#9")
 ("Synopsis of the Principal Games Included" "#13")
 ("Whist and Its Derivatives" "#16")

 ("Contract Bridge" "#21"
                    ("How Contract Bridge is played" "#22")
                    ("The Scoring" "#25")
                    ("Bidding" "#27")
                    ("The Play" "#34")
                    ("Nine Illustrative Deals" "#37")
                    ("Laws and Etiquette" "#46")
                    ("Scoring Table" "#47"))

 ("Duplicate Bridge" "#50")

 ("Booby" "#51"
          ("(Contract Bridge for Three Players)" "#51"))

 ("Solo Whist" "#67"
               ("Some Critical Deals" "#74")
               ("Ten Illustrative Hands" "#84")
               ("General Misère" "#103")
               ("Competitive Misère" "#105")
               ("Solo Whist for Three Players" "#107"))

 ("Piquet" "#109"
               ("An Illustrative Partie" "#112")
               ("Duplicate Piquet" "#120")
               ("Piquet" "#124")
               ("Test Paper for Piquet Players" "#136"))

 ("Cribbage" "#139"
               ("Notes on the Technique of Play" "#145")
               ("6- and 7-Card Cribbage" "#150")
               ("Cribbage for Four Players" "#154")
               ("Auction Cribbage" "#156")
               ("Cribbage for One" "#157"))

 ("Black Maria" "#159"
               ("Black Maria for Three" "#161")
               ("Promlems in Passing Cards and in Play" "#163"))

 ("Poker" "#170"
               ("Illustrative Deals" "#174")
               ("'Pots' at Poker: Jackpot; Freakpot; Acepot" "#178")
               ("Misère Pots" "#184")
               ("Stud Poker" "#186")
               ("Misère Studs" "#190"))

 ("Quintet" "#191")
 ("Canasta" "#193")

 ("Gin Rummy" "#202"
               ("Illustrative Deals" "#204"))

 ("Other Games of The Rummy Family" "#207"
                                    ("Seven-Card Rummy" "#207")
                                    ("Kings and Queens" "#212")
                                    ("Sequence Rummy" "#215")
                                    ("Progressive Rummy" "#217"))

 ("Challenge" "#221"
               ("An Illustrative Game" "#224"))

 ("Other Games for Two Players" "#226"
                                ("German Whist" "#226")
                                ("Nullos" "#227"
                                          ("Contract Nullos" "#230"))
                                ("Bezique" "#232"
                                           ("An Illustrative Deal" "#236"))
                                ("Kalabriasz" "#239")
                                ("Bezique, Poker and Cribbage Patience" "#243"))

 ("Games for Two, Three or Four Players" "#246"
                                         ("Tablanette" "#246")
                                         ("Cassino" "#248"))

 ("Games for Three Players" "#251"
                            ("Five Hundred" "#251")
                            ("Knaves" "#254"))

 ("Games for Four Players" "#257"
                           ("Hasenpfeffer" "#257")
                           ("Greek Hearts" "#258")
                           ("Slobberhannes" "#259"))

 ("Games for From Three to Seven Players" "#262"
                                          ("Brag" "#262")
                                          ("The Four Knaves" "#263")
                                          ("Hitting The Moon" "#264")
                                          ("Napoleon ('Nap')" "#266")
                                          ("Chase The Ace" "#268")
                                          ("Vingt-en-Un (Pontoon)" "#268")
                                          ("Catch-the-Ten" "#273")
                                          ("Newmarket" "#274")
                                          ("Hoggenheimer" "#278"))

 ("Party Games" "#280"
                ("Cheat" "#280")
                ("Rockaway" "#281")
                ("Minoru" "#283")
                ("Racing Demon" "#284")
                ("Authors" "#286")
                ("Great Expectations" "#287")
                ("Parliament" "#288")
                ("Commit" "#289")
                ("On! Well" "#291")
                ("Pip-Pip!" "#292")
                ("Two PuzzlinG Card Party Tricks" "#296"))

 ("Patience Games" "#300"
                   ("Alternations" "#300")
                   ("As You Like It" "#301")
                   ("Carlton" "#302")
                   ("Castles in Spain" "#303")
                   ("Deauville" "#304")
                   ("Demon" "#305")
                   ("Forty-Nine" "#307")
                   ("Kings" "#308")
                   ("Labyrinth" "#310")
                   ("Lady Palk" "#311")
                   ("Les Hurts" "#312")
                   ("Milligan Harp" "#313")
                   ("Monte Carlo" "#314")
                   ("Mystery" "#316")
                   ("One Foundation" "#317")
                   ("Pas Seul" "#318")
                   ("Raglan" "#318")
                   ("Red and Black" "#320")
                   ("Reform" "#321")
                   ("Regiment" "#322")
                   ("Royal Parade" "#323")
                   ("Six By Six" "#325")
                   ("Squadron" "#326")
                   ("Streets" "#327")
                   ("Sultan" "#328")
                   ("Thirty" "#329")
                   ("Triangle" "#330")
                   ("Triple Line" "#332"))
 )
